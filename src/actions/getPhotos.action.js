import {
    GET_PHOTOS,
    GET_PHOTOS_SUCCESS,
    GET_PHOTOS_REJECT,
    SORT_BY_RATING,
    SORT_BY_RATING_SUCCESS
} from "../constants/action-types";
import {AjaxServices} from "../services/ajax.service";

export function getPhotosAction() {
    return (dispatch) => {
        dispatch({
            type: GET_PHOTOS,
        });

        new AjaxServices().read('https://api.myjson.com/bins/9b99d')
            .then(data => JSON.parse(data))
            .then(data => dispatch({
                type: GET_PHOTOS_SUCCESS,
                data,
            }))
            .catch(error => dispatch({
                type: GET_PHOTOS_REJECT,
                error,
            }));
    };
}

export function sortPhotos(typeOfSort) {
    return (dispatch, getState) => {
        dispatch({
            type: SORT_BY_RATING,
            data: getState().photosList.list,
            typeOfSort
        })
    }
}

export function finishSort() {
    return (dispatch) => {
        dispatch({
            type: SORT_BY_RATING_SUCCESS,
        })
    };
}