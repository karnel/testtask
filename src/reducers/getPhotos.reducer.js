import {
    GET_PHOTOS,
    GET_PHOTOS_SUCCESS,
    GET_PHOTOS_REJECT,
    SORT_BY_RATING,
    SORT_BY_RATING_SUCCESS,
    INCREASE_RATING,
    DECREASE_RATING
} from "../constants/action-types";

const initialState = {
    list: [],
    isFetching: false,
    typeOfSort: 'default',
    isSortChanged: false
};

function onGetPhotosSuccess(state, action) {
    return {
        ...state,
        list: action.data,
        isFetching: false,
    };
}

function onIncreaseRating(state, action) {
    return {
        ...state,
        list: increaseRating(action.data, action.event, action.id),
        isFetching: false,
        typeOfSort: action.typeOfSort
    }
}

function onDecreaseRating(state, action) {
    return {
        ...state,
        list: decreaseRating(action.data, action.event, action.id),
        isFetching: false,
        typeOfSort: action.typeOfSort
    }
}

function increaseRating(data, event, id) {
    if (event == 'click') {
        data.forEach((photo) => {
            if (photo.id == id) {
                photo.rating += 0.1;
            }
        });
        return data;
    }
}

function decreaseRating(data, event, id) {
    if (event == '3') {
        data.forEach((photo) => {
            if (photo.id == id) {
                photo.rating -= 0.1;
            }
        });
        return data;
    }
}

function sortPhotos(data, typeOfSort) {
    data = Array.isArray(data) ? data : Array.from(data)
    let list = [];
    switch (typeOfSort) {
        case 'default':
            list = data.sort(function (a, b) {
                if (a.id > b.id) {
                    return 1;
                }
                if (a.id < b.id)
                    return -1;
                else return 0;
            });
            break;
        case 'ask':
            list = data.sort(function (a, b) {
                if (a.rating > b.rating) {
                    return 1;
                }
                if (a.rating < b.rating)
                    return -1;
                else return 0;
            });
            break;
        case 'desk':
            list = data.sort(function (a, b) {
                if (a.rating < b.rating) {
                    return 1;
                }
                if (a.rating > b.rating)
                    return -1;
                else return 0;
            });
    }
    return list;
}

function onSort(state, action) {
    return {
        ...state,
        list: sortPhotos(action.data, action.typeOfSort),
        isFetching: false,
        typeOfSort: action.typeOfSort,
        isSortChanged: true
    }
}


export const getPhotosReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PHOTOS:
            return {
                ...state,
                isFetching: true,
            };
        case GET_PHOTOS_SUCCESS:
            return onGetPhotosSuccess(state, action);
        case GET_PHOTOS_REJECT:
            return {
                ...state,
                list: 'ERROR',
                isFetching: false,
            };
        case SORT_BY_RATING:
            return onSort(state, action);
        case SORT_BY_RATING_SUCCESS:
            return {...state, isSortChanged: false,};
        case INCREASE_RATING:
            return onIncreaseRating(state, action);
        case DECREASE_RATING:
            return onDecreaseRating(state, action);
        default:
            return state;
    }
};
