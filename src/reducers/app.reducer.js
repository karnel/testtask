import { combineReducers } from 'redux';
import {getPhotosReducer} from "./getPhotos.reducer";

export default combineReducers({
    photosList: getPhotosReducer
});