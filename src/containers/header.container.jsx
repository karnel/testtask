import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {HeaderComponent} from "../components/header.component";

export class HeaderContainer extends Component {

    render() {
        return (
            <HeaderComponent/>
        );
    }
}