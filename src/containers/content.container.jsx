import React from 'react';
import { Switch, Route } from 'react-router-dom';
import FlowersComponent from "../components/flowers.component";
import CarsComponent from "../components/cars.component.jsx";

export const ContentContainer = () => (
    <main className="content">
        <Switch>
            <Route exact path="/" component={CarsComponent} />
            <Route path="/flowers" component={FlowersComponent} />
            <Route path="/cars" component={CarsComponent} />
        </Switch>
    </main>
)