class AjaxServices {

    _ajax (method, url, data) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open(method, url, data);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = (err) => {
                try {
                    resolve(xhr.response);
                } catch (err) {
                    reject(err);
                }
            };
            xhr.send(data || null);
        })
    }

    read(url) {
        return this._ajax('GET', url);
    }
    write(url, data){
        return this._ajax('POST', url, data);
    }
}

export { AjaxServices };