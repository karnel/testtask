import { HashRouter, Switch, Route, Link } from 'react-router-dom';
import ReactDOM from 'react-dom';
import React from 'react';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { AppComponent } from './components/app.component';
import configureStore from 'store/configureStore'

const store = configureStore();

ReactDOM.render((
    <Provider store={store}>
      <HashRouter>
        <AppComponent />
      </HashRouter>
    </Provider>
), document.getElementById('root'));