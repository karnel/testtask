import React from 'react';
import {Switch, Route} from 'react-router-dom';
import {HeaderComponent} from "./header.component";
import FlowersComponent from "./flowers.component";
import CarsComponent from "./cars.component.jsx";

export const AppComponent = () => (
    <div>
        <HeaderComponent/>
        <MainContentComponent/>
    </div>
);

const MainContentComponent = () => (
    <main className="content">
        <Switch>
            <Route exact path="/" component={CarsComponent}/>
            <Route path="/flowers" component={FlowersComponent}/>
            <Route path="/cars" component={CarsComponent}/>
        </Switch>
    </main>
);