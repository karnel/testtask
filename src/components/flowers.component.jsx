import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import ReactDOM from 'react-dom';
import React, {Component} from 'react';
import {getPhotosAction} from "../actions/getPhotos.action";
import * as styles from '../styles/items.style.scss'
import {increaseRatingAction, decreaseRatingAction} from "../actions/changeRating.action";
import {sortPhotos, finishSort} from "../actions/getPhotos.action";


class FlowersComponent extends Component {

    constructor() {
        super();
        this.state = ({positionsArray: []});
    }

    componentDidMount() {
        this.props.actions.getPhotosAction();
    }

    increaseRating(event) {
        this.props.actions.increaseRatingAction(event.type, event.target.id);
        this.forceUpdate();
    }

    decreaseRating(event) {
        event.preventDefault();
        this.props.actions.decreaseRatingAction(event.nativeEvent.which, event.target.id);
        this.forceUpdate();
    }

    sortingByRating(event) {
        this.props.actions.sortPhotos(event.target.value);
        let positionsArray = this.getPositions();
        this.setState({positionsArray: positionsArray});
    }

    getPositions() {
        let positionsArray = [];
        for (var i in this.refs) {
            const domNode = ReactDOM.findDOMNode(this.refs[i]);
            const boundingBox = {coordinates: domNode.getBoundingClientRect(), id: this.refs[i].children[0].id};
            positionsArray.push(boundingBox);
        }
        return positionsArray;
    }

    getDiffPositions(prevPositions, currentPositions) {
        let result = [];
        for (var prevPosition of prevPositions) {
            let position = {prevPosition: prevPosition.coordinates, id: prevPosition.id};
            for (let currentPosition of currentPositions) {
                if (prevPosition.id == currentPosition.id) {
                    position.currentPosition = currentPosition.coordinates;
                }
            }
            result.push(position)
        }
        return result;
    }


    componentDidUpdate(prevProps, prevState) {
        if (this.props.photos.isSortChanged) {
            let prevPositions = this.state.positionsArray;
            let currentPositions = this.getPositions();
            let diffPositions = this.getDiffPositions(prevPositions, currentPositions);
            for (var i in diffPositions) {
                const domNode = ReactDOM.findDOMNode(this.refs[i]);
                const oldPosition = diffPositions[i].prevPosition;
                const newPosition = diffPositions[i].currentPosition;
                // console.log("old-new", oldPosition , newPosition);
                let dX = oldPosition.left - newPosition.left;
                let dY = oldPosition.top - newPosition.top;
                if (window.innerWidth >= 600) {
                    requestAnimationFrame(() => {
                        domNode.style.transform = `translate(${-dX}px, ${-dY}px)`;
                        domNode.style.transition = 'transform 0s';
                        requestAnimationFrame(() => {
                            domNode.style.transform = ``;
                            domNode.style.transition = 'transform 2000ms';
                        });
                    });
                }
            }
            setTimeout(() => {
                this.props.actions.finishSort()
            }, 2000);
        }
    }

    render() {
        const photos = this.props.photos.list ? this.props.photos.list : [];
        const flowers = photos.filter(function (flower) {
            return flower.category === 'flowers';
        });
        return (
            <div className={'content-container'}>
                <div className={'content-container__select'}>
                    <span>Sort by:</span>
                    <select onChange={this.sortingByRating.bind(this)}>
                        <option value='default'>Default</option>
                        <option value='ask'>Rating &uarr;</option>
                        <option value='desk'>Rating &darr;</option>
                    </select>
                </div>
                {
                    flowers.map((photo, index) =>
                        <div className={'content-container__item'} id={index * 5} key={Date.now() + index} ref={index}>
                        <span onClick={this.increaseRating.bind(this)} onContextMenu={this.decreaseRating.bind(this)}
                              id={photo.id}
                              className={'content-container__item-rating'}>{photo.rating.toFixed(1)}&#10026;</span>
                            <img className={'content-container__item-image'} src={photo.url}></img>
                            <span className={'content-container__item-category'}>Category: {photo.category}</span>
                        </div>
                    )}
            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({
            getPhotosAction,
            increaseRatingAction,
            decreaseRatingAction,
            sortPhotos,
            finishSort
        }, dispatch),
    }
}

function mapStateToProps(state) {
    return {
        photos: state.photosList
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FlowersComponent);
